#include<iostream>
using namespace std;
class Box{
    public:

    void boxArea(float length, float width,float height){
        cout<<(2*(length*width)+2*(length*height)+2*(height*width))<<endl;
    }
    void boxVolume(float length, float width, float height){
       cout<<(length*width*height)<<endl;
    }
    friend void displayBoxDimensions();
    inline void displayWelcomeMessage();
};
void displayBoxDimensions(Box &obj){
    cout<<"The Dimensions Of The Box Are:"<<endl;
}
inline void Box::displayWelcomeMessage(){
    cout<<"Hello Welcome To My Program"<<endl;
}
int main(){
    Box obj;
    float length,width,height;
    obj.displayWelcomeMessage();
    cout<<"Enter The length, width and height of the Box"<<endl;
    cin>>length>>width>>height;
    obj.boxArea(length,width,height);
    obj.boxVolume(length,width,height);
    displayBoxDimensions();
}

